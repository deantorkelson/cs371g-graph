// -------
// Graph.h
// --------

#ifndef Graph_h
#define Graph_h

// --------
// includes
// --------

#include <cassert> // assert
#include <cstddef> // size_t
#include <utility> // make_pair, pair
#include <vector>  // vector

// -----
// Graph
// -----

class Graph {
public:
    // ------
    // usings
    // ------

    using vertex_descriptor = int;
    using edge_descriptor   = int;

    using vertex_iterator    = int*;
    using edge_iterator      = int*;
    using adjacency_iterator = int*;

    using vertices_size_type = std::size_t;
    using edges_size_type    = std::size_t;

public:
    // --------
    // add_edge
    // --------

    /**
     * Creates a new edge between two vertices and adds it to graph
     * @return a pair containing the new edge and a bool indicating whether the addition was successful
     * (only fails if edge already exists)
     */
    friend std::pair<edge_descriptor, bool> add_edge (vertex_descriptor v1, vertex_descriptor v2, Graph& graph) {
        assert(graph.e_count >= 0);
        edge_descriptor ed = graph.e_count++;
        bool            b  = true;
        return std::make_pair(ed, b);
    }

    // ----------
    // add_vertex
    // ----------

    /**
     * Creates a new vertex and adds it to graph
     * @return the newly created vertex
     */
    friend vertex_descriptor add_vertex (Graph& graph) {
        assert(graph.v_count >= 0);
        return graph.v_count++;
    }

    // -----------------
    // adjacent_vertices
    // -----------------

    /**
     * @returns a pair of adjacency iterators for all the vertices adjacent to v: one for the next, one for the final
     */
    friend std::pair<adjacency_iterator, adjacency_iterator> adjacent_vertices (vertex_descriptor v, const Graph& graph) {
        static int a [] = {0, 0};
        adjacency_iterator b = a;
        adjacency_iterator e = a + 2;
        return std::make_pair(b, e);
    }

    // ----
    // edge
    // ----

    /**
     * If an edge exists between two vertices, return a pair containing <edge, true>
     * otherwise <0, false>
     * @return a pair containing an edge, if it exists, and a bool indicating existence of that edge
     */
    friend std::pair<edge_descriptor, bool> edge (vertex_descriptor v1, vertex_descriptor v2, const Graph& graph) {
        edge_descriptor ed = 0;
        bool            b  = true;
        return std::make_pair(ed, b);
    }

    // -----
    // edges
    // -----

    /**
     * @returns a pair of edge iterators for all edges in the graph:
     *          one for the next, one for the final
     */
    friend std::pair<edge_iterator, edge_iterator> edges (const Graph& graph) {
        static int a [] = {0, 0};
        edge_iterator b = a;
        edge_iterator e = a + 2;
        return std::make_pair(b, e);
    }

    // ---------
    // num_edges
    // ---------

    /**
     * @return the number of edges in graph
     */
    friend edges_size_type num_edges (const Graph& graph) {
        return graph.e_count;
    }

    // ------------
    // num_vertices
    // ------------

    /**
     * @return the current number of vertices in the graph
     */
    friend vertices_size_type num_vertices (const Graph& graph) {
        return graph.v_count;
    }

    // ------
    // source
    // ------

    /**
     * @return the vertex that e points from
     */
    friend vertex_descriptor source (edge_descriptor e, const Graph& graph) {
        assert(e <= graph.e_count);
        vertex_descriptor v = e-1;
        return v;
    }

    // ------
    // target
    // ------

    /**
     * @return the vertex pointed to by edge e
     */
    friend vertex_descriptor target (edge_descriptor e, const Graph& graph) {
        assert(e <= graph.e_count);
        vertex_descriptor v = e;
        return v;
    }

    // ------
    // vertex
    // ------

    /**
     * @return a vertex descriptor pointing to the "vth" vertex in graph
     */
    friend vertex_descriptor vertex (vertices_size_type v, const Graph& graph) {
        assert(v <= graph.v_count);
        vertex_descriptor vd = v;
        return vd;
    }

    // --------
    // vertices
    // --------

    /**
     * @returns a pair of vertex iterators for all the vertices in the graph:
     *          one for the next, one for the final
     */
    friend std::pair<vertex_iterator, vertex_iterator> vertices (const Graph& graph) {
        static int a [] = {0, 0};
        vertex_iterator b = a;
        vertex_iterator e = a + 2;
        return std::make_pair(b, e);
    }

private:
    // ----
    // data
    // ----

    std::vector<std::vector<vertex_descriptor>> g;
    vertices_size_type v_count = 0;
    edges_size_type e_count = 0;

    // -----
    // valid
    // -----

    /**
     * Checks if the graph is currently valid
     * @return true if valid, false otherwise
     */
    bool valid () const {
        assert(e_count >= 0);
        assert(v_count >= 0);
        return e_count >= 0 && v_count >= 0;
    }

public:
    // --------
    // defaults
    // --------

    Graph             ()             = default;
    Graph             (const Graph&) = default;
    ~Graph            ()             = default;
    Graph& operator = (const Graph&) = default;
};

#endif // Graph_h
