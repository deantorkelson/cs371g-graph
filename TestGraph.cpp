// -------------
// TestGraph.c++
// -------------

// https://www.boost.org/doc/libs/1_73_0/libs/graph/doc/index.html

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <iterator> // ostream_iterator
#include <sstream>  // ostringstream
#include <utility>  // pair

#include "boost/graph/adjacency_list.hpp" // adjacency_list

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// ---------
// TestGraph
// ---------

template <typename G>
struct GraphFixture : Test {
    // ------
    // usings
    // ------

    using graph_type          = G;
    using vertex_descriptor   = typename G::vertex_descriptor;
    using edge_descriptor     = typename G::edge_descriptor;
    using vertex_iterator     = typename G::vertex_iterator;
    using edge_iterator       = typename G::edge_iterator;
    using adjacency_iterator  = typename G::adjacency_iterator;
    using vertices_size_type  = typename G::vertices_size_type;
    using edges_size_type     = typename G::edges_size_type;
};

// directed, sparse, unweighted
// possibly connected
// possibly cyclic
using
graph_types =
    Types<
    boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>
    // ,Graph
    >;

#ifdef __APPLE__
TYPED_TEST_CASE(GraphFixture, graph_types,);
#else
TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

TYPED_TEST(GraphFixture, num_vertices0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertices_size_type size = num_vertices(g);
    ASSERT_EQ(size, 0);
}

TYPED_TEST(GraphFixture, num_edges0) {
    using graph_type         = typename TestFixture::graph_type;
    using edges_size_type = typename TestFixture::edges_size_type;

    graph_type g;

    edges_size_type size = num_edges(g);
    ASSERT_EQ(size, 0);
}

TYPED_TEST(GraphFixture, num_edges1) {
    using graph_type         = typename TestFixture::graph_type;
    using edges_size_type = typename TestFixture::edges_size_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor a = add_vertex(g);

    add_edge(a, a, g);

    edges_size_type size = num_edges(g);
    ASSERT_EQ(size, 1);
}

TYPED_TEST(GraphFixture, num_edges2) {
    using graph_type         = typename TestFixture::graph_type;
    using edges_size_type = typename TestFixture::edges_size_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor a = add_vertex(g);

    for (int i = 0; i < 10; ++i)
        add_edge(a, a, g);

    edges_size_type size = num_edges(g);
    ASSERT_EQ(size, 1);
}

TYPED_TEST(GraphFixture, edge_success_true) {
    using graph_type         = typename TestFixture::graph_type;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g;

    std::pair<edge_descriptor, bool> e = add_edge(add_vertex(g), add_vertex(g), g);

    ASSERT_TRUE(e.second);
}

TYPED_TEST(GraphFixture, edge_success_false) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);

    std::pair<edge_descriptor, bool> e = add_edge(a, b, g);
    std::pair<edge_descriptor, bool> e2 = add_edge(a, b, g);

    ASSERT_TRUE(e.second);
    ASSERT_FALSE(e2.second);
}

TYPED_TEST(GraphFixture, edge_detect) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);

    add_edge(a, b, g);
    std::pair<edge_descriptor, bool> edge_true = edge(a, b, g);
    std::pair<edge_descriptor, bool> edge_false = edge(b, a, g);

    ASSERT_TRUE(edge_true.second);
    ASSERT_FALSE(edge_false.second);
}

TYPED_TEST(GraphFixture, add_vertex0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;


    graph_type g;

    vertex_descriptor a = add_vertex(g);

    ASSERT_EQ(a, vertex(0, g));
}

TYPED_TEST(GraphFixture, add_vertex1) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;


    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);
    vertex_descriptor c = add_vertex(g);

    ASSERT_EQ(a, vertex(0, g));
    ASSERT_EQ(b, vertex(1, g));
    ASSERT_EQ(c, vertex(2, g));
}

// add vertex
TYPED_TEST(GraphFixture, add_5_vertex) {
    using graph_type         = typename TestFixture::graph_type;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    for (int i = 0; i < 5; ++i) {
        add_vertex(g);
    }

    vertices_size_type size = num_vertices(g);
    ASSERT_EQ(size, 5);
}

TYPED_TEST(GraphFixture, add_1000_vertex) {
    using graph_type         = typename TestFixture::graph_type;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    for (int i = 0; i < 1000; ++i) {
        add_vertex(g);
    }

    vertices_size_type size = num_vertices(g);
    ASSERT_EQ(size, 1000);
}

// add edge
TYPED_TEST(GraphFixture, add_1_edge) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);

    std::pair<edge_descriptor, bool> e = add_edge(a, b, g);

    ASSERT_TRUE(e.second);
    ASSERT_EQ(num_edges(g), 1);
}

TYPED_TEST(GraphFixture, add_duplicate_edge) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);

    std::pair<edge_descriptor, bool> e = add_edge(a, b, g);
    std::pair<edge_descriptor, bool> e2 = add_edge(a, b, g);

    ASSERT_TRUE(e.second);
    ASSERT_FALSE(e2.second);
    ASSERT_EQ(num_edges(g), 1);
}

TYPED_TEST(GraphFixture, add_bidir_edge) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);

    std::pair<edge_descriptor, bool> e = add_edge(a, b, g);
    std::pair<edge_descriptor, bool> e2 = add_edge(b, a, g);

    ASSERT_TRUE(e.second);
    ASSERT_TRUE(e2.second);
    ASSERT_EQ(num_edges(g), 2);
}

// adjacent vertices
TYPED_TEST(GraphFixture, count_adjacent_vertices0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);
    vertex_descriptor c = add_vertex(g);

    add_edge(a, b, g);
    add_edge(a, c, g);

    std::pair<adjacency_iterator, adjacency_iterator> adjacencies = adjacent_vertices(a, g);

    int adj_count = 0;
    while (adjacencies.first != adjacencies.second) {
        ++adj_count;
        ++adjacencies.first;
    }

    ASSERT_EQ(adj_count, 2);
}

TYPED_TEST(GraphFixture, count_adjacent_vertices1) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);
    vertex_descriptor c = add_vertex(g);

    add_edge(a, b, g);
    add_edge(c, a, g);

    std::pair<adjacency_iterator, adjacency_iterator> adjacencies = adjacent_vertices(a, g);

    int adj_count = 0;
    while (adjacencies.first != adjacencies.second) {
        ++adj_count;
        ++adjacencies.first;
    }

    ASSERT_EQ(adj_count, 1);
}

TYPED_TEST(GraphFixture, count_adjacent_vertices2) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);

    add_edge(b, a, g);

    std::pair<adjacency_iterator, adjacency_iterator> adjacencies = adjacent_vertices(a, g);

    int adj_count = 0;
    while (adjacencies.first != adjacencies.second) {
        ++adj_count;
        ++adjacencies.first;
    }

    ASSERT_EQ(adj_count, 0);
}

TYPED_TEST(GraphFixture, count_many_adjacent_vertices) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    for (int i = 0; i < 500; ++i) {
        vertex_descriptor other = add_vertex(g);
        add_edge(a, other, g);
        add_edge(other, a, g);
    }

    std::pair<adjacency_iterator, adjacency_iterator> adjacencies = adjacent_vertices(a, g);

    int adj_count = 0;
    while (adjacencies.first != adjacencies.second) {
        ++adj_count;
        ++adjacencies.first;
    }

    ASSERT_EQ(adj_count, 500);
}

TYPED_TEST(GraphFixture, source0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);
    std::pair<edge_descriptor, bool> e = add_edge(a, b, g);

    ASSERT_EQ(source(e.first, g), a);
}

TYPED_TEST(GraphFixture, source1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);
    pair<edge_descriptor, bool> e = add_edge(a, b, g);
    pair<edge_descriptor, bool> e2 = add_edge(b, a, g);

    ASSERT_EQ(a, source(e.first, g));
    ASSERT_EQ(b, source(e2.first, g));
}

TYPED_TEST(GraphFixture, target0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g;
    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);
    std::pair<edge_descriptor, bool> e = add_edge(a, b, g);

    ASSERT_EQ(target(e.first, g), b);
}

TYPED_TEST(GraphFixture, target1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);

    pair<edge_descriptor, bool> e = add_edge(a, b, g);
    pair<edge_descriptor, bool> e2 = add_edge(b, a, g);

    ASSERT_EQ(b, target(e.first, g));
    ASSERT_EQ(a, target(e2.first, g));
}

TYPED_TEST(GraphFixture, short_chain) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);
    vertex_descriptor c = add_vertex(g);
    vertex_descriptor d = add_vertex(g);
    add_edge(a, b, g);
    add_edge(b, c, g);
    add_edge(c, d, g);

    int index = 0;
    std::pair<vertex_iterator, vertex_iterator> verts = vertices(g);
    while (verts.first != verts.second) {
        ASSERT_EQ(vertex(index, g), *verts.first);
        ++index;
        ++verts.first;
    }
}

TYPED_TEST(GraphFixture, vertices0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g;

    for (int i = 0; i < 10; ++i) {
        add_vertex(g);
    }

    int index = 0;
    std::pair<vertex_iterator, vertex_iterator> verts = vertices(g);
    while (verts.first != verts.second) {
        ASSERT_EQ(vertex(index, g), *verts.first);
        ++index;
        ++verts.first;
    }
    ASSERT_EQ(index, 10);
}

TYPED_TEST(GraphFixture, vertices1) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g;

    for (int i = 0; i < 10000; ++i) {
        add_vertex(g);
    }

    int index = 0;
    std::pair<vertex_iterator, vertex_iterator> verts = vertices(g);
    while (verts.first != verts.second) {
        ASSERT_EQ(vertex(index, g), *verts.first);
        ++index;
        ++verts.first;
    }
    ASSERT_EQ(index, 10000);
}

TYPED_TEST(GraphFixture, oscillation) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);
    add_edge(a, b, g);
    add_edge(b, a, g);

    vertex_descriptor iter = vertex(0, g);
    for (int i = 0; i < 100; ++i) {
        iter = *(adjacent_vertices(iter, g).first);
    }
    ASSERT_EQ(iter, vertex(0, g));
}

TYPED_TEST(GraphFixture, check_target_connections) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b;
    for (int i = 0; i < 5; ++i) {
        b = add_vertex(g);
        add_edge(a, b, g);
        a = b;
    }

    vertex_descriptor start = vertex(0, g);
    vertex_descriptor next = vertex(1, g);
    vertex_descriptor nextnext = vertex(2, g);
    vertex_descriptor nextnextnext = vertex(3, g);
    ASSERT_EQ(*adjacent_vertices(start, g).first, next);
    ASSERT_EQ(*adjacent_vertices(next, g).first, nextnext);
    ASSERT_EQ(*adjacent_vertices(nextnext, g).first, nextnextnext);
}

TYPED_TEST(GraphFixture, traverse_by_adjacency) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor start = vertex(0, g);
    vertex_descriptor b;
    for (int i = 0; i < 1000; ++i) {
        b = add_vertex(g);
        add_edge(a, b, g);
        a = b;
    }
    add_edge(b, start, g);
    vertex_descriptor iter = vertex(1, g);
    for (int i = 0; i < 1000; ++i) {
        iter = *(adjacent_vertices(iter, g).first);
    }
    ASSERT_EQ(iter, start);
}

TYPED_TEST(GraphFixture, traverse_very_long_chain) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor start = vertex(0, g);
    vertex_descriptor b;
    for (int i = 0; i < 1000000; ++i) {
        b = add_vertex(g);
        add_edge(a, b, g);
        a = b;
    }
    add_edge(b, start, g);
    vertex_descriptor iter = vertex(1, g);
    for (int i = 0; i < 1000000; ++i) {
        iter = *(adjacent_vertices(iter, g).first);
    }
    ASSERT_EQ(iter, start);
}

TYPED_TEST(GraphFixture, multi_test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);

    edge_descriptor e = add_edge(a, b, g).first;

    pair<edge_descriptor, bool> ab_edge = add_edge(a, b, g);
    ASSERT_EQ(ab_edge.first,  e);
    ASSERT_FALSE(ab_edge.second);

    edges_size_type size = num_edges(g);
    ASSERT_EQ(size, 1);

    ASSERT_EQ(source(e, g), a);
    ASSERT_EQ(target(e, g), b);
}

TYPED_TEST(GraphFixture, iter_test0) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> iter_pair = vertices(g);
    ASSERT_NE(iter_pair.first, iter_pair.second);

    ASSERT_EQ(*iter_pair.first, a);
    ++iter_pair.first;
    ASSERT_EQ(*iter_pair.first, b);
    ++iter_pair.first;
    ASSERT_EQ(iter_pair.first, iter_pair.second);
}

